<?php if (!empty($page['content'])): ?>
  <div id="main-content" class="row medium-uncollapse large-collapse">
      	<div class="large-4 large-centered columns">
            
            	<p style="text-align: center;"><a href="http://advocacy.manilawater.com"><img src="<?php print $logo ?>" alt="<?php print $site_name ?> | <?php print $site_slogan ?>" title="<?php print $site_name ?> | <?php print $site_slogan ?>" id="logo" /></a></p>
                
                <?php print $messages; ?>
                <?php print render($page['help']); ?>

                <?php if ($tabs): ?>
                  <div class="tabs"><?php /*print render($tabs);*/ ?></div>
                <?php endif; ?>
                
                <?php if ($action_links): ?>
                  <ul class="action-links"><?php print render($action_links); ?></ul>
                <?php endif; ?>
                
                <?php print render($page['content']); ?>
        </div>
  </div>
<?php endif; ?>