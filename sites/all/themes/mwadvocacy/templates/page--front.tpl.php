<section class="off-canvas-wrap" data-offcanvas>
  <div class="inner-wrap">
    <aside class="left-off-canvas-menu">
      <?php print render($page['left_off_canvas']); ?>
    </aside>    
    
    <aside class="right-off-canvas-menu">
      <?php print render($page['right_off_canvas']); ?>
    </aside>

        <div id="header" class="row div-vertical-separator">
          <div class="large-12 columns text-center">
              <?php if (!empty($page['header'])): ?>
                    <?php print render($page['header']); ?>
              <?php endif; ?>
            <a href="http://advocacy.manilawater.com"><img src="<?php print $logo ?>" alt="<?php print t('Manila Water') ?>" id="logo" /></a>
          </div>
        </div>
        
        
		<?php if (!empty($page['banner'])): ?>
        <div id="banner" class="row small-collapse large-uncollapse div-vertical-separator">
			<div class="large-12 columns text-center">
				<?php print render($page['banner']); ?>
			</div>
        </div>
		<?php endif; ?>
        
        
		<?php if (!empty($page['navigation'])): ?>
        <div id="navigation" class="row div-vertical-separator">
			<div class="large-12 columns text-center">
				<?php print render($page['navigation']); ?>
            </div>
        </div>
		<?php endif; ?>
      
      
      <?php if ($messages): ?>  
      <div class="l-messages row">
        <div class="large-12 columns">
          <?php print $messages; ?>
        </div>
      </div>
      <?php endif; ?>
      

      <?php if (!empty($page['help'])): ?>
      <div class="l-help row div-vertical-separator">
        <div class="large-12 columns">
          <?php print render($page['help']); ?>
        </div>
      </div>
	  <?php endif; ?>


	  <?php if (!empty($page['content'])): ?>
      <div class="row relative">
        <div id="main" class="small-12 columns">
          <?php if (!empty($page['highlighted'])): ?>
              <?php print render($page['highlighted']); ?>
          <?php endif; ?>
          
          <?php //if ($title): ?>
			<!--h1 class="title"><?php //print $title; ?></h1-->
          <?php //endif; ?>

		  <?php if ($tabs): ?>
            <div class="tabs"><?php print render($tabs); ?></div>
          <?php endif; ?>
          
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
          
          <?php print render($page['content']); ?>
        </div>
	    <?php endif; ?>
        
        
        <?php if (!empty($page['side_first'])): ?>
          <div id="side-first" class="small-12 medium-6 large-6 columns sidebar div-vertical-separator">
          	<div class="sidebar-inner">
            	<?php print render($page['side_first']); ?>
			</div>
            <div id="side-first-footer"></div>
        	<div id="mobile-kuya-pat-toka-lakbayan"></div>
          </div>
	    <?php endif; ?>
        
        
        <?php if (!empty($page['side_second'])): ?>
          <div id="side-second" class="small-12 medium-6 large-6 columns sidebar div-vertical-separator">
            <div class="sidebar-inner">
            	<?php print render($page['side_second']); ?>
			</div>
            <div id="side-second-footer"></div>
        	<div id="mobile-kuya-pat-toka-toka"></div>
          </div>
	    <?php endif; ?>
        
        <div id="kuya-pat"></div>
        
        <?php if (!empty($page['pre_footer'])): ?>
          <div id="pre-footer" class="small-12 columns pre-footer">
            	<?php print render($page['pre_footer']); ?>
          </div>
	    <?php endif; ?>
        
      </div>
      
      
      <?php if (!empty($page['footer_first']) or !empty($page['footer_second'])): ?>
        <footer>
        	<div class="row">
              <?php if (!empty($page['footer_first'])): ?>
                <div id="footer-first" class="small-12 medium-8 large-8 columns">
                  <?php print render($page['footer_first']); ?>
                </div>
              <?php endif; ?>
              <?php if (!empty($page['footer_second'])): ?>
                <div id="footer-second" class="small-12 medium-4 large-4 columns">
                  <?php print render($page['footer_second']); ?>
                </div>
              <?php endif; ?>
          	</div>
        </footer>
	    <?php endif; ?>

    <a class="exit-off-canvas"></a>

  </div>
</section>