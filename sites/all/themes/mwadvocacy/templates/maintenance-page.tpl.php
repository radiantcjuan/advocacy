<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>" style="height: 100%;">

<head>
  <title><?php //print $head_title; ?>Manila Water Advocacy | Coming Soon</title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body id="maintenance-body" class="<?php print $classes; ?>" style="height: 100%; width: 100%;">
	<div style="display: table; height: 100%; width: 100%;">
    	<div style="display: table-cell; height: 100%; width: 100%; vertical-align: middle; text-align: center;">
			<img style="margin-bottom: 7px;" src="http://reports.manilawater.com/resources/public/files/ManilaWater_Logo_Portrait.png" />
            <p style="text-align: center; font-family: 'Bebas Neue'; font-size: 24px; color: #717171; display: inline;"><br /><strong>Advocacy</strong></p>
            <p style="text-align: center; font-family: 'Bebas Neue'; font-size: 20px; color: #019fce;"><br /><strong>Coming Soon.</strong></p>
        </div>
    </div>
</body>
</html>
