<nav class="top-bar" data-topbar role="navigation">
  <ul class="title-area">
    <li class="name">
      <div id="logo"><a href="http://advocacy.manilawater.com/" title="<?php print t('Manila Water Advocacy'); ?>" rel="home"><img src="<?php print $logo ?>" alt="<?php print t('Manila Water Advocacy') ?>" id="logo-image" /></a></div>
    </li>
     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
    <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
  </ul>

  <section class="top-bar-section">
    <!-- Right Nav Section -->
    <ul class="right">
		 <?php
          /*$menu = menu_navigation_links('main-menu');
          print theme('links__menu_your_custom_menu_name', array('links' => $menu));*/
          ?>
          
          <?php print render($page['right_off_canvas']); ?>
      <!--li class="active"><a href="#">Right Button Active</a></li>
      <li class="has-dropdown">
        <a href="#">Right Button Dropdown</a>
        <ul class="dropdown">
          <li><a href="#">First link in dropdown</a></li>
          <li class="active"><a href="#">Active link in dropdown</a></li>
        </ul>
      </li-->
    </ul>

    <!-- Left Nav Section -->
    <!--ul class="left">
      <li><a href="#">Left Nav Button</a></li>
    </ul-->
  </section>
</nav>




<div id="header" class="row div-vertical-separator">
  <div class="large-12 columns text-center">
      <?php if (!empty($page['header'])): ?>
            <?php print render($page['header']); ?>
      <?php endif; ?>
    <a href="http://advocacy.manilawater.com"><img src="<?php print $logo ?>" alt="<?php print t('Manila Water') ?>" id="logo" /></a>
  </div>
</div>


<?php if (!empty($page['banner'])): ?>
<div id="banner" class="row small-collapse large-uncollapse div-vertical-separator">
    <div class="large-12 columns text-center">
        <?php print render($page['banner']); ?>
    </div>
</div>
<?php endif; ?>


<?php if (!empty($page['navigation'])): ?>
<div id="navigation" class="row collapse">
    <div class="large-12 columns text-center">
        <?php print render($page['navigation']); ?>
    </div>
</div>
<?php endif; ?>


<?php if (!empty($page['help'])): ?>
<div class="l-help row collapse div-vertical-separator">
<div class="large-12 columns">
  <?php print render($page['help']); ?>
</div>
</div>
<?php endif; ?>


<?php if (!empty($page['highlighted'])): ?>
<div id="highlighted" class="row collapse div-vertical-separator">
    <div class="large-12 columns text-center">
        <?php print render($page['highlighted']); ?>
    </div>
</div>
<?php endif; ?>


<?php if (!empty($page['content'])): ?>
<div class="row relative">
<div id="toka-toka-main-top" class="small-12 columns"></div>
<div id="main" class="small-12 columns">

  <?php if ($title): ?>
    <h1 class="title"><?php print $title; ?></h1>
  <?php endif; ?>
    
  <?php if ($messages): ?>  
  <div class="l-messages row collapse">
  <div class="large-12 columns">
    <?php print $messages; ?>
  </div>
  </div>
  <?php endif; ?>

  <?php if ($tabs): ?>
    <div class="tabs"><?php print render($tabs); ?></div>
  <?php endif; ?>
  
  <?php if ($action_links): ?>
    <ul class="action-links"><?php print render($action_links); ?></ul>
  <?php endif; ?>
  
  <?php print render($page['content']); ?>
</div>
<div id="toka-toka-main-bottom" class="small-12 columns div-vertical-separator"></div>
</div>
<?php endif; ?>


<?php if (!empty($page['side_first']) or !empty($page['side_second'])): ?>
  <div class="row collapse relative">
      <div id="side-first" class="small-12 medium-4 large-4 columns sidebar div-vertical-separator">
        <div class="sidebar-inner">
            <?php print render($page['side_first']); ?>
        </div>
      </div>
      
      <div id="side-second" class="small-12 medium-8 large-8 columns sidebar div-vertical-separator">
        <div class="sidebar-inner">
            <?php print render($page['side_second']); ?>
        </div>
      </div>
  </div>
<?php endif; ?>


<?php if (!empty($page['pre_footer'])): ?>
  <div id="pre-footer" class="row relative collapse div-vertical-separator">
    <div class="small-12 columns pre-footer">
        <?php print render($page['pre_footer']); ?>
    </div>
  </div>
<?php endif; ?>


<?php if (!empty($page['footer_first']) or !empty($page['footer_second'])): ?>
<footer>
    <div class="row">
      <?php if (!empty($page['footer_first'])): ?>
        <div id="footer-first" class="small-12 medium-8 large-8 columns">
          <?php print render($page['footer_first']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($page['footer_second'])): ?>
        <div id="footer-second" class="small-12 medium-4 large-4 columns">
          <?php print render($page['footer_second']); ?>
        </div>
      <?php endif; ?>
    </div>
</footer>
<?php endif; ?>