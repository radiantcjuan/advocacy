jQuery(window).on("load", function($) {
	
	jQuery('body.front #side-first').hover(function() {
		jQuery('body.front #side-first .front-lakbayan-description .block-image img').attr("src","http://advocacy.manilawater.com/resources/public/files/imageblock/lakbayan-logo-hover.png");
	}, function() {
		jQuery('body.front #side-first .front-lakbayan-description .block-image img').attr("src","http://advocacy.manilawater.com/resources/public/files/imageblock/lakbayan-logo.png");
	});
	
	jQuery('body.front #side-second').hover(function() {
		jQuery('body.front #side-second .front-tokatoka-description .block-image img').attr("src","http://advocacy.manilawater.com/resources/public/files/imageblock/toka-toka-logo-hover.png");
	}, function() {
		jQuery('body.front #side-second .front-tokatoka-description .block-image img').attr("src","http://advocacy.manilawater.com/resources/public/files/imageblock/toka-toka-logo.png");
	});
	
	

	jQuery('body.front #side-first').click(function(a) {
		window.location.href = "http://advocacy.manilawater.com/lakbayan";
	});
	
	jQuery('body.front #side-second').click(function(a) {
		window.location.href = "http://advocacy.manilawater.com/toka-toka";
	});
	


	/* Add height to thumb of home video of toka-toka */
	var TokaTokaHomeVideoHeight = jQuery('body.section-toka-toka #side-second .toka-toka-home-video .album-image img').height();

	
	jQuery('body.section-toka-toka #side-second .toka-toka-home-video .click-here-text').css('height',TokaTokaHomeVideoHeight);

	jQuery(window).resize(function() {
		/* Set height of play button to 100% of window height */
		var TokaTokaHomeVideoHeightResize = jQuery('body.section-toka-toka #side-second .toka-toka-home-video .album-image img').height();
		jQuery('body.section-toka-toka #side-second .toka-toka-home-video .click-here-text').css('height',TokaTokaHomeVideoHeightResize);
	});



var width = jQuery(window).width();
	if ((width >= 768)) {		
		
		var maxHeight = -1;
			  var HeightAdjust = 25;
		  
			  jQuery('body.section-lakbayan.lakbayan-news .lakbayan-news-sub .views-field-title-2, body.section-toka-toka.toka-toka-news .toka-toka-news-sub .views-field-title-2').each(function() {
				  maxHeight = maxHeight > jQuery(this).height() ? maxHeight : jQuery(this).height();
			  });
			  
			  jQuery('body.section-lakbayan.lakbayan-news .lakbayan-news-sub .views-field-title-2, body.section-toka-toka.toka-toka-news .toka-toka-news-sub .views-field-title-2').each(function() {
				  jQuery(this).height(maxHeight + HeightAdjust);
			  });
		  
			  jQuery('body.section-toka-toka #side-second').each(function() {
				  maxHeight = maxHeight > jQuery(this).height() ? maxHeight : jQuery(this).height();
			  });
			  
			  jQuery('body.section-toka-toka #side-first').each(function() {
				  jQuery(this).height(maxHeight);
			  });
		}	  
		
		
		jQuery('body.section-lakbayan .lakbayan-picture-upload').click(function(e) {
			jQuery('body.section-lakbayan .lakbayan-picture-upload h2').fadeOut( 500, function() {
				jQuery('body.section-lakbayan #side-second .sidebar-inner .lakbayan-picture-upload form').animate({
					opacity: 1
				  }, 500, function() {
					// Animation complete.
				  }).css("visibility","visible");
				
				jQuery('body.section-lakbayan #side-second .sidebar-inner .lakbayan-picture-upload form, body.section-lakbayan .lakbayan-picture-upload h2').css("cursor","default");
			});
		});
				  
		jQuery('body.section-lakbayan .lakbayan-signup').click(function(e) {
			jQuery('body.section-lakbayan .lakbayan-signup h2').fadeOut( 500, function() {
				jQuery('body.section-lakbayan #side-second .sidebar-inner .lakbayan-signup form').animate({
					opacity: 1
				  }, 500, function() {
					// Animation complete.
				  }).css("visibility","visible");
				  
				jQuery('body.section-lakbayan #side-second .sidebar-inner .lakbayan-signup form, body.section-lakbayan .lakbayan-signup h2').css("cursor","default");
			});
		});
				  
				  
				  
				  
		jQuery('body.section-toka-toka .toka-toka-picture-upload').click(function(e) {
			jQuery('body.section-toka-toka .toka-toka-picture-upload h2').fadeOut( 500, function() {
				jQuery('body.section-toka-toka #side-second .sidebar-inner .toka-toka-picture-upload form').animate({
					opacity: 1
				  }, 500, function() {
					// Animation complete.
				  }).css("visibility","visible");
				  
				jQuery('body.section-lakbayan #side-second .sidebar-inner .lakbayan-signup form, body.section-lakbayan .lakbayan-signup h2').css("cursor","default");
			});
		});
		
		
		
		
			
	var LakbayanHomeGalleryURL = jQuery('body.section-lakbayan .lakbayan-overview-gallery-background .block-image img').attr("src");
	var LakbayanHomeGalleryBackground = "background: url("+LakbayanHomeGalleryURL+") no-repeat top center / cover;";
	
	jQuery('body.section-lakbayan #side-first .sidebar-inner').attr("style", LakbayanHomeGalleryBackground);


		
var width = jQuery(window).width();
	if ((width >= 1000)) {
		  var maxHeight = -1;
		  var HeightAdjust = 50;
		  var AddHeight = 200;
		  var SideFooter = jQuery('#side-first-footer').height();
	  
		  jQuery('.front .sidebar .sidebar-inner').each(function() {
			  maxHeight = maxHeight > jQuery(this).height() ? maxHeight : jQuery(this).height();
		  });
		  
		  jQuery('.front .sidebar .sidebar-inner').each(function() {
			  jQuery(this).height(maxHeight + AddHeight);
		  });
		  
		  /*jQuery('.front-lakbayan-description, .front-tokatoka-description').each(function() {
			  jQuery(this).height(maxHeight + SideFooter + AddHeight + HeightAdjust);
		  });*/
		  
		  jQuery('.front-lakbayan-description div.block-body, .front-tokatoka-description div.block-body').each(function() {
			  jQuery(this).height(maxHeight + SideFooter + AddHeight);
		  });
		  
		  jQuery('#side-first').hover(function($) {
			  jQuery('#kuya-pat').css("background","url('sites/all/themes/mwadvocacy/images/kuya-pat1.png') no-repeat center center / 100% 100%");
			  jQuery('#kuya-pat').css("left","25%");
		  });
		  
		  jQuery('#side-second').hover(function($) {
			  jQuery('#kuya-pat').css("background","url('sites/all/themes/mwadvocacy/images/kuya-pat2.png') no-repeat center center / 100% 100%");
			  jQuery('#kuya-pat').css("left","31%");
		  });
		  
		  
		  /* Lakbayan Side */
		  var SecondSideHeight = jQuery('.section-lakbayan #side-second').height();
		  var HeightAdjustForFirstSide = 40;
		  
		  jQuery('.section-lakbayan #side-first .sidebar-inner').height(SecondSideHeight - HeightAdjustForFirstSide);
		  
		  
	} else if ((width >= 768)) {
		
		  var maxHeight = -1;
		  var HeightAdjust = 0;
		  var AddHeight = 0;
		  var SideFooter = jQuery('#side-first-footer').height();
	  
		  jQuery('.front .sidebar .sidebar-inner').each(function() {
			  maxHeight = maxHeight > jQuery(this).height() ? maxHeight : jQuery(this).height();
		  });
		  
		  jQuery('.front .sidebar .sidebar-inner').each(function() {
			  jQuery(this).height(maxHeight - 50);
		  });
		  
		  /*jQuery('.front-lakbayan-description, .front-tokatoka-description').each(function() {
			  jQuery(this).height(maxHeight);
		  });*/
		  
		  jQuery('.front-lakbayan-description div.block-body, .front-tokatoka-description div.block-body').each(function() {
			  jQuery(this).height(maxHeight - 50);
		  });
		  
		  jQuery('#side-first').hover(function($) {
			  jQuery('#kuya-pat').css("background","url('sites/all/themes/mwadvocacy/images/kuya-pat1.png') no-repeat center center / 100% 100%");
			  jQuery('#kuya-pat').css("left","23%");
		  });
		  
		  jQuery('#side-second').hover(function($) {
			  jQuery('#kuya-pat').css("background","url('sites/all/themes/mwadvocacy/images/kuya-pat2.png') no-repeat center center / 100% 100%");
			  jQuery('#kuya-pat').css("left","35%");
		  });
	} else if ((width <= 767)){
		
		  var maxHeight = 0;
		  var HeightAdjust = 100;
		  var AddHeight = 200;
		  var SideFooter = 0;
	  
		  jQuery('.front .sidebar .sidebar-inner').each(function() {
			  maxHeight = maxHeight > jQuery(this).height() ? maxHeight : jQuery(this).height();
		  });
		  
		  jQuery('.front .sidebar .sidebar-inner').each(function() {
			  jQuery(this).css("height", maxHeight + SideFooter + AddHeight);
		  });
		  
		  /*jQuery('.front-lakbayan-description, .front-tokatoka-description').each(function() {
			  jQuery(this).height(maxHeight);
		  });*/
		  
		  jQuery('.front-lakbayan-description div.block-body, .front-tokatoka-description div.block-body').each(function() {
			  jQuery(this).height(maxHeight + AddHeight - 30);
		  });
		  
		  jQuery('#side-first').hover(function($) {
			  jQuery('#kuya-pat').css("background","url('sites/all/themes/mwadvocacy/images/kuya-pat1.png') no-repeat center center / 100% 100%");
			  jQuery('#kuya-pat').css("left","23%");
		  });
		  
		  jQuery('#side-second').hover(function($) {
			  jQuery('#kuya-pat').css("background","url('sites/all/themes/mwadvocacy/images/kuya-pat2.png') no-repeat center center / 100% 100%");
			  jQuery('#kuya-pat').css("left","35%");
		  });
		
	}

});





