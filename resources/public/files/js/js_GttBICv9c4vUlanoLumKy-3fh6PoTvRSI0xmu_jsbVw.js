(function ($) {
  Drupal.behaviors.qtip = {
    attach: function (context, settings) {
      $(function () {
        var instances = $.parseJSON(settings.instances);
        var debug = $.parseJSON(settings.qtipDebug);

        $('.qtip-link', context).each(function() { // Call .each() so we can access $(this) in the settings/config
          // Check for a sibling .qtip-tooltip containing the tooltip information, otherwise (for forms)
          // go to the element's parent and look for .qtip-tooltip container as a sibling of the parent
          if ($(this, context).next('.qtip-tooltip').length) {
            var tooltipElement = $(this).next('.qtip-tooltip');
          }
          else {
            var tooltipElement = $(this).parent().siblings('.qtip-tooltip');
          }
          var tooltip = tooltipElement;

          if (!debug.leaveElement) {
            // Remove the tooltip element to keep the DOM clean
            tooltipElement.remove();
          }

          var text     = tooltip.html();
          var title    = (tooltip.data('qtip-title') != undefined) ? tooltip.data('qtip-title') + '' : ''; // Concatenate an empty string to make sure that the value being passed as the title is a string, otherwise it will not display
          var instance = (tooltip.data('qtip-instance') != undefined) ? tooltip.data('qtip-instance') : '';
          var settings = (instances[instance] != undefined) ? instances[instance] : '';

          if (settings) {
            $(this).qtip(settings);
            $(this).qtip('option', 'content.text', text);
            if (title) {
              $(this).qtip('option', 'content.title', title);
            }
          }
          // If no settings have been passed through, we still want to display a tooltip
          else {
            $(this).qtip({
              content: {
                text: text,
                title: title,
              }
            });
          }

          // Add instance class to the container to aid in styling
          $(this).addClass('qtip-instance-' + instance);
        });
      });
    }
  };
})(jQuery);
;
/**
 * @file
 * Adds some show/hide to the admin form to make the UXP easier.
 */
(function($){
  Drupal.behaviors.video = {
    attach: function (context, settings) {
      //lets see if we have any jmedia movies
      if($.fn.media) {
        $('.jmedia').media();
      }
	
      if(settings.video) {
        $.fn.media.defaults.flvPlayer = settings.video.flvplayer;
      }
	
      //lets setup our colorbox videos
      $('.video-box').each(function() {
        var url = $(this).attr('href');
        var data = $(this).metadata();
        var width = data.width;
        var height= data.height;
        var player = settings.video.player; //player can be either jwplayer or flowplayer.
        $(this).colorbox({
          html: '<a id="video-overlay" href="'+url+'" style="height:'+height+'; width:'+width+'; display: block;"></a>',
          onComplete:function() {
            if(player == 'flowplayer') {
              flowplayer("video-overlay", settings.video.flvplayer, {
                clip: {
                  autoPlay: settings.video.autoplay,
                  autoBuffering: settings.video.autobuffer
                }
              });
            } else {
              $('#video-overlay').media({
                flashvars: {
                  autostart: settings.video.autoplay
                },
                width:width,
                height:height
              });
            }
          }
        });
      });
    }
  };

  // On change of the thumbnails when edit.
  Drupal.behaviors.videoEdit = {
    attach : function(context, settings) {
      function setThumbnail(widget, type) {
        var thumbnails = widget.find('.video-thumbnails input');
        var defaultthumbnail = widget.find('.video-use-default-video-thumb');
        var largeimage = widget.find('.video-preview img');

        var activeThumbnail = thumbnails.filter(':checked');
        if (activeThumbnail.length > 0 && type != 'default') {
          var smallimage = activeThumbnail.next('label.option').find('img');
          largeimage.attr('src', smallimage.attr('src'));
          defaultthumbnail.attr('checked', false);
        }
        else if(defaultthumbnail.is(':checked')) {
          thumbnails.attr('checked', false);
          largeimage.attr('src', defaultthumbnail.data('defaultimage'));
        }
        else {
          // try to select the first thumbnail.
          if (thumbnails.length > 0) {
            thumbnails.first().attr('checked', 'checked');
            setThumbnail(widget, 'thumb');
          }
        }
      }

      $('.video-thumbnails input', context).change(function() {
        setThumbnail($(this).parents('.video-widget'), 'thumb');
      });

      $('.video-use-default-video-thumb', context).change(function() {
        setThumbnail($(this).parents('.video-widget'), 'default');
      });

      $('.video-widget', context).each(function() {
        setThumbnail($(this), 'both');
      });
    }
  }
})(jQuery);
;
